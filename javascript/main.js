
var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

function do_register() {

    var username = document.getElementById("username");
    var password = document.getElementById("pass1");
    var repeat_password = document.getElementById("pass2");
    var name   = document.getElementById("name");
    var family = document.getElementById("family");
    var phone  = document.getElementById("phone");
    var email  = document.getElementById("email");
    var man    = document.getElementById("gender1");
    var woman  = document.getElementById("gender2");
    var city   = document.getElementById("city");
    var rule   = document.getElementById("acp");


    var star1 = document.getElementById("star1");
    var star2 = document.getElementById("star2");
    var star3 = document.getElementById("star3");
    var star4 = document.getElementById("star4");
    var star5 = document.getElementById("star5");
    var star6 = document.getElementById("star6");
    var star7 = document.getElementById("star7");
    var star8 = document.getElementById("star8");
    var star9 = document.getElementById("star9");
    var star11= document.getElementById("star11");


    if (username.value == ""){
        star1.style.display = "block";
        username.focus();
        return false
    }else if (password.value == ""){
        star2.style.display = "block";
        password.focus();
        return false
    }else if (password.value != repeat_password.value){
        star3.style.display = "block";
        repeat_password.focus();
        return false
    }else if (name.value == ""){
        star4.style.display = "block";
        name.focus();
        return false
    }else if (family.value == ""){
        star5.style.display = "block";
        family.focus();
        return false
    }else if (phone.value == ""){
        star6.style.display = "block";
        phone.focus();
        return false
    }else if (email.value == ""){
        star7.style.display = "block";
        email.focus();
        return false
    }else if (filter.test(email.value == false)){
        star7.style.display = "block";
        email.focus();
        return false
    }else if (!man.checked && !woman.checked){
        star8.style.display = "block";
        return false
    }else if (city.value == 0){
        star9.style.display = "block";
        return false
    }else if (!rule.checked){
        star11.style.display = "block";
        return false
    }


}//End of function do_register

function redirect(page) {
    window.location.href = page;
}//End of function redirect


function show_time() {

    window.setTimeout("show_time()",1000);

    var time = new Date();
    document.getElementById('time').innerHTML =time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
}//End of function show_time


function up_down(id,type) {
    var obj = document.getElementById(id);
    var val = parseInt(obj.value);

    if (type == 1){
        // increment
        val += 1;
        obj.value = val;
    }else if (type == 0){
        if (val == 1){
            obj.value = val;
        }else {
            val -= 1;
            obj.value = val;
        }
    }
}//End of function up_down


function find_title() {
    var text   = document.getElementById("text_find").value;

    if (text == ""){
        alert("لطفا برای جستجتو عبارتی را وارد کنید .")
    }else {
        redirect("index.php?menu_id=-6&title="+text);
    }
}//End of function find_title



function open_win(url){
    $win = window.open(url,"win","Height=600,Width=400");
    $win.focus();

}//End of function open_win


function del(pp){
    var conf = confirm('آيا ميخواهيد اين رکورد حذف شود ؟');
    if(conf){
        redirect(pp);
    }

}//End of function del
