<div align="center"><b id="b1">فرم ثبت نام</b></div>
<div align="center" style="font-family: 'B Nazanin' , Tahoma ; font-size: 18pt">
    <span style="color: red" id="text">
            <?php
                if ($message == "no_user"){
                    print "لطفا نام کاربری را وارد کنید";
                }elseif ($message == "no_pass"){
                    print "لطفا کلمه عبور را وارد کنید";
                }elseif ($message == "no_name"){
                    print "لطفا نام خود را وارد کنید";
                }elseif ($message == "no_lname"){
                    print "لطفا نام خانوادگی خود را وارد کنید";
                }elseif ($message == "no_phone"){
                    print "لطفا شماره تلفن خود را وارد کنید";
                }elseif ($message == "novalidphone"){
                    print "لطفا شماره تلفن معتبر خود را وارد کنید";
                }elseif ($message == "no_email"){
                    print "لطفا پست الکترونیکی خود را وارد کنید";
                }elseif ($message == "no_emailval"){
                    print "لطفا پست الکترونیکی معتبر را وارد کنید";
                }elseif ($message == "nogender"){
                    print "لطفا جنسیت را وارد کنید";
                }elseif ($message == "no_city"){
                    print "لطفا شهر خود را وارد کنید";
                }elseif ($message == "error1"){
                    print "نام کاربری یا ایمیل یا شماره تلفن تکراری است";
                }
            ?>
    </span>
    <span style="color: green">
           <?php
               if ($message == "success"){
                   print  "  ثبت نام شما با موفقیت صورت گرفت و شماره پیگیری شما ". $_GET['uid'] ."می باشد";
               }
           ?>
    </span>
</div>


<form method="post" onsubmit="return do_register()">
    <table align="center" dir="rtl" id="t1">
        <tr>
            <td><label for="username">نام کاربری :</label></td>
            <td><input type="text" id="username" name="username" maxlength="50" value="<?php print $_SESSION["username"] ?>"></td>
            <td class="star" id="star1">*</td>
        </tr>
        <tr>
            <td><label for="pass1">کلمه عبور :</label></td>
            <td><input type="password" id="pass1" name="pass1" maxlength="20"></td>
            <td class="star" id="star2">*</td>
        </tr>
        <tr>
            <td><label for="pass2">تکرار کلمه عبور :</label></td>
            <td><input type="password" id="pass2" name="pass2" maxlength="20"></td>
            <td class="star" id="star3">*</td>
        </tr>
        <tr>
            <td><label for="name">نام :</label></td>
            <td><input type="text" id="name" name="name" maxlength="20" value="<?php print $_SESSION["name"] ?>"></td>
            <td class="star" id="star4">*</td>
        </tr>
        <tr>
            <td><label for="last_name">نام خانوادگی :</label></td>
            <td><input type="text" id="last_name" name="last_name" maxlength="20" value="<?php print $_SESSION["family"] ?>"></td>
            <td class="star" id="star5">*</td>
        </tr>
        <tr>
            <td><label for="phone">تلفن تماس :</label></td>
            <td><input type="text" id="phone" name="phone" maxlength="50" value="<?php print $_SESSION["phone"] ?>"></td>
            <td class="star" id="star6">*</td>
        </tr>
        <tr><td><label for="email">پست الکترونیکی :</label></td><td><input type="text" id="email" name="email" maxlength="50" value="<?php print $_SESSION["email"] ?>"></td><td class="star" id="star7">*</td></tr>
        <tr><td>جنسیت :</td>
            <td>
                <label>مرد :<input type="radio" id="gender1" name="gender" value="1" checked="checked"></label>
                <label>  زن :<input type="radio" id="gender2" name="gender" value="2" <?php
                    if ($_SESSION['gender'] == 2){
                        print 'checked="checked"';
                    }?>></label>
            </td>
            <td class="star" id="star8">*</td>
        </tr>
        <tr><td>شهر :</td>
            <td>
                <select id="city" name="city">
                    <option value="0">لطفا شهر خود را انتخاب کنید .</option>
                    <?php
                    $result_city = $main->all_city();
                    while ($cities = mysqli_fetch_assoc($result_city)){
                    ?>
                    <option
                        <?php if ($_SESSION["city"] == $cities["id"]) print "selected = selected"?>
                            value="<?php print $cities["id"] ?>"><?php print $cities["title"] ?>
                    </option>
                    <?php  } //end of while  ?>
                </select>
            </td>
            <td class="star" id="star9">*</td>
        </tr>
        <tr><td>قوانین :</td>
            <td>
                <textarea readonly="readonly">
                    1) محتوای سایت مطعلق به مصطفی کاشفی است
                    2)هر گونه کپی برداری از سایت پیگرد قانونی دارد
                </textarea>
            </td>
        </tr>
        <tr><td colspan="2"><label for="acp">با قوانین سایت موافقت می کنم .</label><input type="checkbox" id="acp" name="acp" value="1"></td><td class="star" id="star11">*</td></tr>
        <tr>
            <td colspan="2">
                <input type="submit" id="register" name="register"  value="ثبت نام">
                <input type="reset" value="دوباره">
            </td>
        </tr>
    </table>
</form>


<br><br><br><br><br><br><br><br>
<div style="width: 100%; color: black; text-align: center; font-size: 10pt; font-weight: bold" >
    ---------------------------------------------------------------------<br>
    محتویات این سایت مطعلق به مصطفی کاشفی است<br>
    و هرگونه کپی برداری پیگرد قانونی دارد
</div>

