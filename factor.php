<h3 align="center" style="font-weight: bold;">فاکتور خرید شما</h3>
<table dir="rtl" align="center"  border="1" cellspacing="0" cellpadding="0" width="650" style="line-height:25px" valign="top">

    <tr align="center" style="font-weight: bold; font-size: 13pt;">
        <td> شماره </td>
        <td> نام محصول </td>
        <td> قیمت </td>
        <td> تعداد </td>
        <td> قیمت کل هر محصول </td>
    </tr>
    <?php
    $result_list_basket = $main->list_user_basket();
    $counter = 1;
    $sum[] = 0;
    $number_buy[] = 0;
    while ($rows = mysqli_fetch_assoc($result_list_basket)) {
        $info_pro = $main->more_info_product($rows['pro_id']);
        $sum[] = ($info_pro['price']*$rows['number']) ;
        $number_buy[] = $rows['number'] ;
        ?>
        <tr align="center">
            <td> <?php print $counter?> </td>
            <td> <?php print $info_pro['title'] ?> </td>
            <td> <?php print $info_pro['price'] ?> </td>
            <td> <?php print $rows['number'] ?></td>
            <td> <?php print $info_pro['price']*$rows['number'] ?> </td>
        </tr>

        <?php
        $counter++;
    }
    ?>
</table>
<div align="center" style="font-family: 'B Nazanin' , Tahoma ; font-size: 18pt; color:darkred ">
    <br>
    <?php
    $total_sum = array_sum($sum);
    $total_number = array_sum($number_buy);

    print "تعداد خرید شما : ".($total_number);
    print "<br>";

    if ($total_sum >= 500000 || $total_number >= 5){
        $dic = ($total_sum*10)/100;
        print "مبلغ کل خرید شما با احتساب 10% تخفیف : ".($total_sum-$dic)." تومان";
    }else{
        print "مبلغ کل خرید شما : ".($total_sum)." تومان";
    }
    ?>
    <br>
    <br>
    <a href="index.php?menu_id=-4" title="return">بازگشت</a>
    <br>
    <br>

    <?php
    if (isset($_POST['send_message_factor'])){
        $full_name_factor = $_POST['full_name_factor'];
        $email_factor     = $_POST['email_factor'];
        $phone_factor     = $_POST['phone_factor'];
        $address_factor   = $_POST['address_factor'];

        $result_factor = $main->insert_order($full_name_factor,$email_factor,$phone_factor,$address_factor);
         if ($full_name_factor=="" || $email_factor=="" ||$phone_factor=="" ||$address_factor==""){
             $main->Redirect_html("index.php?menu_id=-5&msg=complete_form");
         }elseif ($result_factor){
             $main->Redirect_html("index.php?menu_id=-5&msg=send_factor");
         }else{
            $main->Redirect_html("index.php?menu_id=-5&msg=not_send_factor");
         }
    }

    ?>
    <div align="center"><b id="b1">فرم اطلاعات</b></div>
    <div align="center" style="font-family: 'B Nazanin' , Tahoma ; font-size: 18pt">
    <span style="color: red" id="text">
        <?php

        if($message == "not_send_factor") {
            print "سفارش شما ثبت نشد";
        }?>
    </span>
        <span style="color: green">
        <?php

        if ($message == "send_factor") {
            print "سفارش شما ثبت شد";
        }

        ?>
    </span>
        <span style="color: red">
        <?php

        if ($message == "complete_form") {
            print "لطفا فرم را تکمیل کنید";
        }

        ?>
    </span>
    </div>
    <form method="post">
        <table align="center" dir="rtl" id="t1">
            <tr>
                <td>نام و نام خانوادگی :</td>
                <td><input type="text" id="full_name_factor" name="full_name_factor" maxlength="20"></td>
            </tr>
            <tr>
                <td>پست الکترونیکی :</td>
                <td><input type="text" id="email_factor" name="email_factor" maxlength="50" ></td>
            </tr>
            <tr>
                <td>تلفن تماس :</td>
                <td><input type="text" id="phone_factor" name="phone_factor" maxlength="20"></td>
            </tr>
            <tr>
                <td>آدرس : </td>
                <td>
                    <textarea style="width: 200px ; height: 100px" id="address_factor" name="address_factor"></textarea>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <input type="submit" id="send_message_factor" name="send_message_factor"  value="ارسال ">
                </td><td></td>
            </tr>
        </table>
    </form>
</div>
<br><br>
<div style="width: 100%; color: black; text-align: center; font-size: 10pt; font-weight: bold" >
    ---------------------------------------------------------------------<br>
    محتویات این سایت مطعلق به مصطفی کاشفی است<br>
    و هرگونه کپی برداری پیگرد قانونی دارد
</div>