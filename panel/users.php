<?php
session_start();
include_once("../include/config.php");
$main = new online_store();
if(!isset($_SESSION['admin_id'])){
    $main->redirect_html("index.php");
}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>نمایش اطاعات کاربران</title>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../javascript/main.js"></script>
</head>
<style type="text/css">
    body{
        font-family: 'B Nazanin' , Tahoma;
        background-color: #cccccc;
        font-size: 14pt;
    }

    a{
        text-decoration: none;
        color: black;
    }


</style>

<body>

<?php
if($task=='edit'){
    //edit
    $info = $main->info_users_admin($edit);
    if($_POST['save']){

        $email  = $_POST['email'];
        $name   = $_POST['name'];
        $family = $_POST['family'];
        $phone  = $_POST['phone'];

        $rs = $main->Save_users($edit,$email,$name,$family,$phone);

        if($rs){
            $main->redirect_html("?msg=ok&page=$page&task=edit&edit=$edit");
        }else{
            $main->redirect_html("?msg=err&page=$page&task=edit&edit=$edit");
        }

    }




    ?>

    <form method="post" action="">
        <table align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center"><h2>نمایش اطاعات کاربران</h2></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center">
                    <?php

                    if($message=='ok'){
                        print "<div style='font-size: 18pt; color: green'>با موفقيت ذخیره شد</div> ";
                    }elseif($message=='err'){
                        print  "<div style='font-size: 18pt; color: red'>با موفقيت ذخیره نشد</div> ";
                    }

                    ?>


                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>

            <tr>
                <td>
                    نام  :
                    <input type="text" name="name" value="<?php print $info['name'] ?>"  id="name"   class="input_text" />

                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td>
                    نام خانوادگی  :‌
                    <input type="text" name="lastname" value="<?php print $info['family'] ?>"  id="lastname" dir="rtl" class="input_text" /></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>

            <tr>
                <td>
                    پست الکترونیک  :‌
                    <input type="text" name="email" value="<?php print $info['email'] ?>"  id="email" dir="ltr" class="input_text" />

                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>

            <tr>
                <td>
                    شماره تماس  :‌
                    <input type="text" name="tel" value="<?php print $info['phone'] ?>"  id="tel" dir="ltr" class="input_text" />

                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr><td height="20"></td></tr>

            <tr>
                <td  align="center">
                    <input type="submit"  name="save" value="ذخيره" class="input_button" />
                    <input type="button" value="بازگشت" class="input_button" onclick="redirect('?page=<?php print $page ?>');"  />
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
        </table>
    </form>


    <?php
}else{
    //view
    $show = $main->Show_All_users($page);
    if($_GET['del']=='del'){

        $rd = $main->del_users($_GET['did']);
        if($rd){
            $main->redirect_html("?msg=ok_del&page=$page");
        }else{
            $main->redirect_html("?msg=err_del&page=$page");
        }

    }

    ?>
    <form method="post" action="">
        <table width="900" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center"><h2>نمایش اطاعات کاربران</h2> </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center">
                    <?php

                    if($message=='ok_del'){
                        print "<div style='font-size: 18pt; color: green'>با موفقيت حذف شد</div> ";
                    }elseif($message=='err_del'){
                        print  "<div style='font-size: 18pt; color: red'>با موفقيت حذف نشد</div> ";
                    }

                    ?>


                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">



                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>

                            <td width="10"></td>
                            <td align="center">نام</td>
                            <td width="10"></td>
                            <td align="center">نام خانوادگی</td>
                            <td width="10"></td>

                            <td align="center">عمليات</td>
                            <td width="10"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <?php
                        while ($rows = mysqli_fetch_assoc($show[0])){
                            ?>

                            <tr>

                                <td width="15"></td>
                                <td bgcolor="#CC99FF" align="center"><?php print $rows['name'] ?></td>
                                <td width="15"></td>
                                <td bgcolor="#CC99FF" align="center"><?php print $rows['family'];?></td>

                                <td width="15"></td>
                                <td>

                                    <input type="button" value="حذف" class="input_button" onclick="del('?del=del&did=<?php print $rows['id'] ?>&page=<?php print $page ?>');" />
                                    <input type="button" value="ويرايش" class="input_button" onclick="redirect('?task=edit&page=<?php print $page ?>&edit=<?php print $rows['id'] ?>');" />
                                </td>
                                <td width="10"></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <?php
                        }
                        ?>

                    </table>

                    <div style="height:20px"></div>
                    <div id="nav">
                        <?php
                        print " صفحه <b>$page</b>  از <b>$show[1]</b> ";
                        ?>
                        <div style="height:10px;"></div>
                        <?php
                        if($page < $show[1]){
                            $next = ($page + 1);
                            ?>
                            <img src="../images/next.jpg" height="25"  style="cursor:pointer" onclick="redirect('?page=<?php print $next ?>')" width="25" alt="صفحه بعدي" title="صفحه بعدي" />
                            <?php
                        }


                        ?>
                        <?php
                        if($page > 1){
                            $prev = ($page - 1);
                            ?>
                            &nbsp;
                            <img src="../images/back.jpg" height="25"  style="cursor:pointer" onclick="redirect('?page=<?php print $prev ?>')" width="25" alt="صفحه قبلي" title="صفحه قبلي" />
                            <?php
                        }
                        ?>

                    </div>
                    <div style="height:10px;"></div>

                    <input type="button" value="بازگشت" class="input_button" onclick="redirect('index.php');"  />


                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
        </table>
    </form>

    <?php
}
?>
</body>
</html>