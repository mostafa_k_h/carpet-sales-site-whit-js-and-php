<?php
session_start();
include_once("../include/config.php");
$main = new online_store();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ويرايش كردن  محصولات</title>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../javascript/main.js"></script>
    <script type="text/javascript" src="../editor/jscripts/tiny_mce/tiny_mce.js"></script>
   <!-- <script language="javascript">
        tinyMCE.init({
            mode : "textareas",
            theme : "advanced",
            skin : "o2k7",
            plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
            theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            content_css : "css/content.css",
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    </script>
-->
    <style type="text/css">
        body{
            font-family: 'B Nazanin' , Tahoma;
            background-color: #cccccc;
            font-size: 14pt;
        }

        a{
            text-decoration: none;
            color: black;
        }


    </style>


</head>

<body>
<?php
if($task=='edit'){
    //edit
    $info = $main->info_pro_admin($edit);

    if($_POST['save']){

        $title = $_POST['title'];
        $price = $_POST['price'];
        $disc1 = $_POST['disc1'];
        $disc2 = $_POST['disc2'];
        $pic   = $_POST['pic'];


        $rs = $main->Save_pro($edit,$title,$price,$disc1,$disc2,$pic);

        if($rs){
            $main->redirect_html("?msg=ok&page=$page&task=edit&edit=$edit");
        }else{
            $main->redirect_html("?msg=err&page=$page&task=edit&edit=$edit");
        }

    }

    ?>

    <form method="post" action="">
        <table width="900" align="center" dir="rtl">
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center"><h3>ويرايش كردن محصولات</h3></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center">
                    <?php

                    if($message=='ok'){
                        print "<div align='center' style='font-size: 18pt'><b style='color:#2effc0'> با موفقيت ذخیره شد</b></div>";
                    }elseif($message=='err'){
                        print "<div align='center' style='font-size: 18pt'><b style='color:#ff919f'> ذخیره صورت نگرفت</b></div>";
                    }

                    ?>


                </td>
            </tr>


            <tr>
                <td height="10"></td>
            </tr>

            <tr>
                <td  align="center">
                    عنوان محصول :‌
                    <input type="text" name="title"  value="<?php print $info['title']?>" id="title" size="50"  class="input_text" />

                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">
                    توضیحات  مختصر محصول :‌<br />
                    <textarea id="disc1" name="disc1" cols="100" style="width:500px;height: 100px"><?php print $info['disc1']?></textarea>


                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">
                    توضیحات  کامل محصول :‌<br />
                    <textarea id="disc2" name="disc2" cols="100" style="width:500px;height: 100px"><?php print $info['disc2']?></textarea>

                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">
                    قیمت محصول  :‌
                    <input type="text"  name="price"  value="<?php print $info['price']?>"  id="price" dir="ltr" size="50" />

                </td></tr>


            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">
                    عكس محصول  :‌
                    <input type="text" name="pic" value="<?php print $info['pic']?>"  id="pic" dir="ltr" size="50" />
                    <input type="button" value="ارسال فايل" onclick="open_win('upload_file.php')" />

                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>


            <tr>
                <td  align="center">
                    <input type="submit"  name="save" value="ذخيره"  />
                    <input type="button" value="بازگشت" onclick="redirect('?page=<?php print $page ?>');"  />
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
        </table>
    </form>


    <?php
}else{
    //view

    if($_GET['delete']=='del'){

        $rd =$main->del_pro($_GET['did']);

        if($rd){
            $main->redirect_html("?msg=ok_del");
        }else{
            $main->redirect_html("?msg=err_del");
        }

    }

    ?>
    <form method="post">

        <table width="900" align="center">
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center"><h3><br/><br/>
                        ويرايش كردن محصولات</h3> </td>
            </tr>

            <tr>
                <td align="center">
                    <?php

                    if($message=='ok_del'){
                        print "<div align='center' style='font-size: 18pt'><b style='color:#2effc0'> با موفقيت حذف شد</b></div>";
                    }elseif($message=='err_del'){
                        print "<div align='center' style='font-size: 18pt'><b style='color:#ff919f'>حذف صورت نگرفت  </b></div>";
                    }

                    ?>


                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">


                    <?php
                    $show_id = $_GET['show_id'];
                    $show_val = $_GET['show_val'];
                    if($show_id){

                        $q = "UPDATE `tbl_product` SET `show` = '$show_val' WHERE `id` = '$show_id'";
                        $r = mysqli_query($main->db,$q);
                        if($r){
                            $main->redirect_html("?msg=ok_show");
                        }else{
                            $main->redirect_html("?msg=err_show");
                        }

                    }

                    ?>




                    <table align="center" dir="rtl">
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td width="10"></td>

                            <td align="center">عنوان محصول</td>
                            <td width="10"></td>
                            <td align="center">وضیعت</td>
                            <td width="10"></td>
                            <td align="center">عمليات</td>
                            <td width="10"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <?php
                        $show= $main->Show_All_pro();
                        while ($rows = mysqli_fetch_assoc($show)){
                            ?>

                            <tr>

                                <td width="10"></td>
                                <td bgcolor="#DDF99B" align="center"><?php print $rows['title'] ?></td>
                                <td width="10"></td>

                                <td>

                                    <?php

                                    if($rows['show']){

                                        ?>
                                        <a href="?show_id=<?php print $rows['id'] ?>&show_val=0">
                                            <img  border="0" src="../images/a.png" height="25" width="25" />
                                        </a>
                                        <?php
                                    }else{
                                        ?>
                                        <a href="?show_id=<?php print $rows['id'] ?>&show_val=1">
                                            <img border="0" src="../images/d.png" height="25" width="25" />
                                        </a>
                                        <?php
                                    }

                                    ?>

                                </td>
                                <td width="10"></td>

                                <td>

                                    <input type="button" value="حذف"  onclick="redirect('?delete=del&did=<?php print $rows['id'] ?>');" />

                                    <input type="button" value="ويرايش" onclick="redirect('?task=edit&edit=<?php print $rows['id'] ?>');" />
                                </td>
                                <td width="10"></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <?php
                        } // End of while
                        ?>

                        <tr><td align="center" colspan="2">
                                <input type="button" value="بازگشت" class="input_button" onclick="redirect('index.php');"  />


                            </td>
                        </tr>

                        <tr>
                            <td height="10"></td>
                        </tr>
                    </table>
    </form>

    <?php
} //End of else
?>
</body>
</html>