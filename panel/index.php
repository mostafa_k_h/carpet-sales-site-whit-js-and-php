<?php
session_start();
include_once("../include/config.php");
$main = new online_store();

?>

<!DOCTYPE html PUBLIC "~//W3C//DTD XHTML 1.0 Strict//EN">
<html lang="en">
<head xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <script language="javascript" type="text/javascript" src="../javascript/main.js"></script>
    <title>online_shop_panel.ir</title>
    <style type="text/css">
        body{
            font-family: 'B Nazanin' , Tahoma;
            background-color: #cccccc;
            font-size: 15pt;
        }

        a{
            text-decoration: none;
            color: black;
        }


    </style>
</head>
<body>

<?php
if(isset($_SESSION['admin_id'])){
$user = $main->Info_Admin($_SESSION['admin_id']);

if($task=='logout'){
    session_unset();
    session_destroy();
    $main->redirect_html("index.php");
}



if($task=='edit'){

if($_POST['save']){

    $session_admin = $_SESSION['admin_id'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $u = $main->save_profile_admin($session_admin,$username,$password);
    if($u){
        $main->redirect_html("?task=edit&msg=ok");
    }else{
        $main->redirect_html("?task=edit&msg=err");
    }
}
?>

<!--Update user and pass Admin-->
<br /><br /><br />
<form action="" method="post" dir="rtl">


    <table width="550" align="center"  cellspacing="0" cellpadding="0" style="background-color: white; border-radius: 10px">



        <tr><td height="30"></td> </tr>


        <tr><td align="center"><b style="background-color: #aaaaaa; ">تغيير نام كاربري و كلمه عبور</b></td></tr>

        <tr><td height="30"></td> </tr>

        <td align="center">

        </td></tr>

        <tr><td height="30"></td> </tr>


        <tr><td align="center">
                نام كاربري : <input type="text" name="username" id="username" value="<?php print $user['username'] ?>" />
            </td></tr>



        <tr><td align="center">
                كلمه عبور : <input type="password" name="password" id="password"  />
            </td></tr>

        <tr><td height="30"></td> </tr>


        <tr><td align="center">
                <input type="submit" name="save" value="ذخيره"  />
                <input type="button" value="بازگشت"  onclick="redirect('index.php');" />

            </td></tr>

    </table>
    <?php

    if($message=='ok'){
        print "<div align='center' style='font-size: 18pt'><b style='color:#2effc0'> با موفقيت ویرایش شد</b></div>";
    }elseif($message=='err'){
        print "<div align='center' style='font-size: 18pt'><b style='color:#ff919f'>  ویرایش صورت نگرفت</b></div>";
    }
    ?>

</form>

    <?php
        }else{
    ?>

<!-- After Login-->
    <br /><br /><br />
<table align="center" width="750"  cellspacing="0" cellpadding="0" style="background-color: white; border-radius: 10px">


    <tr><td height="40" align="center">

            <marquee width="180"  direction="right" >** به پنل مدیریت خوش آمدید **</marquee>

        </td></tr>

    <tr><td align="center"><img src="../images/001.jpg" /></td></tr>

    <tr><td height="30"></td></tr>


    <tr><td>

            <table align="center" width="500" dir="rtl" cellspacing="0" cellpadding="0">

                <tr><td width="200">
                        <img src="../images/web1.png" />&nbsp;<a href="../index.php"  target="_blank">وب سایت</a></td>

                    <td width="185">
                        <img src="../images/adim.png" />&nbsp;<a href="index.php?task=edit" title="تغيير رمز عبور">تغيير رمز عبور</a></td>

                    <td width="115"><img src="../images/cancel1.png" />&nbsp;<a href="index.php?task=logout" title="خروج"> خروج</a>
                    </td></tr>


            </table>

        </td></tr>

    <tr><td height="30"></td></tr>

    <tr><td align="center"><img src="../images/002.jpg" /></td></tr>

    <tr><td height="30"></td></tr>


    <tr><td>
            <table align="center" width="500" cellspacing="0" cellpadding="0" dir="rtl" >

                <tr><td align="right">
                        <img src="../images/add_pro.png" /> <a href="add_pro.php" >اضافه کردن محصولات</a></td>


                    <td align="right">
                        <img src="../images/edit_pro.png" /><a href="edit_pro.php" > ویرایش محصولات </a></td>


                <tr><td height="30"></td></tr>

                <tr><td align="right">
                        <img src="../images/users.png"/> <a href="users.php?page=1" >نمایش اطاعات کاربران</a></td>

                    <td align="right">
                        <img src="../images/order.png" /> <a href="orders.php?page=1" >سفارشات کاربران</a></td>

                </tr>


                <tr><td height="30"></td></tr>


            </table>

        </td></tr>


</table>


</table>

    <?php
      }//end of if
    ?>

<!--login-->

<?php
}else{
		if($_POST['login']) {
            //login
            $username = $_POST['username'];
            $password = $_POST['password'];
            $r = $main->Login_Admin($username,$password);
            if ($r == 1) {
                $main->redirect_html("index.php");
            } else {
                $main->redirect_html("?msg=err1");
            }
        }

?>

<form action="" method="post">
    <br /><br /><br /><br /><br /><br /><br />
    <?php

    if($message=='err1'){
       print "<script>alert('نام کاربری یا کلمه عبور اشتباه است')</script>";
    }

    ?>

    <table align="center" dir="rtl" style="background-color: #666666; border-radius: 10px;" >



        <tr><td align="center"><img src="../images/hv.png" /></td></tr>



        <tr><td align="center">
                نام كاربري : <input type="text"  id="username" name="username"/>
            </td></tr>



        <tr><td align="center">
                كلمه عبور : <input type="password" id="password" name="password"  />
            </td></tr>



        <tr> <td align="center">
                <input type="submit" id="login" name="login" value="ورود"  />
            </td></tr>


    </table>

</form>
<?php
 }
?>
</body>
</html>
