<?php

session_start();
include_once("../include/config.php");
$main = new online_store();
if($_POST['add']){

    $title = $_POST['title'];
    $disc1 = $_POST['disc1'];
    $disc1 = htmlspecialchars($disc1,ENT_QUOTES);
    $disc2 = $_POST['disc2'];
    $disc2 = htmlspecialchars($disc2,ENT_QUOTES);
    $price = $_POST['price'];
    $pic = $_POST['pic'];

    if ($title == "" ||$price == "" ||$disc2 == "" || $disc1 =="" || $pic ==""){
        $main->redirect("?msg=err");
    }else {

        $q = "INSERT INTO `tbl_product` VALUES (NULL ,'$title','$price',100,'$disc2','$disc1','$pic','1')";
        $r = mysqli_query($main->db, $q);
        if ($r) {
            $main->redirect("?msg=ok");
        } else {
            $main->redirect("?msg=err");
        }
    }

}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>اضافه كردن محصولات</title>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../javascript/main.js"></script>
    <script type="text/javascript" src="../editor/jscripts/tiny_mce/tiny_mce.js"></script>
 <!--   <script language="javascript">
        tinyMCE.init({
            mode : "textareas",
            theme : "advanced",
            skin : "o2k7",
            plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
            theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            content_css : "css/content.css",
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    </script>
    -->
    <style type="text/css">
        body{
            font-family: 'B Nazanin' , Tahoma;
            background-color: #cccccc;
            font-size: 14pt;
        }

        a{
            text-decoration: none;
            color: black;
        }


    </style>

</head>

<body>
<form method="post" >
    <table width="900" align="center" dir="rtl">


        <tr><td align="center"><h2><br/><br/>اضافه كردن محصولات</h2></td></tr>

        <tr><td height="10"></td></tr>


        <tr><td align="center">
                <?php

                if($message=='ok'){
                    print "<div style='font-size: 18pt; color: green'>با موفقيت ثبت شد</div> ";
                }elseif($message=='err'){
                    print  "<div style='font-size: 18pt; color: red'>با موفقيت ثبت نشد</div> ";
                }
                ?>
            </td></tr>






        <tr><td  align="center">

                عنوان محصول : <input type="text" name="title"  id="title" size="50"  />

            </td></tr>

        <tr><td height="10"></td></tr>


        <tr><td  align="center" dir="rtl">
                توضیحات  مختصر محصول :‌‌<br />
                <textarea id="disc1" name="disc1" cols="100" style="width:500px; height: 100px"></textarea>

            </td> </tr>

        <tr><td height="10"></td></tr>

        <tr><td  align="center" dir="rtl">
                توضیحات  کامل محصول :‌‌‌<br />
                <textarea id="disc2" name="disc2" cols="100" style="width:500px;height: 100px"></textarea>

            </td></tr>
        <tr><td height="10"></td></tr>



        <tr><td  align="center">

                قیمت محصول  :‌ <input type="text"  name="price"  id="price" dir="ltr" size="50"  />

            </td></tr>


        <tr><td height="10"></td></tr>


        <tr><td  align="center">

                عكس محصول  :‌<input type="text" name="pic"  id="pic" dir="ltr" size="50" />
                <input type="button" value="ارسال فايل" onclick="open_win('upload_file.php')"  />

            </td></tr>


        <tr><td height="10"></td></tr>


        <tr><td  align="center">
                <input type="submit"  name="add" value="ثبت"  />
                <input type="button" value="بازگشت" onclick="redirect('index.php');"  />
            </td></tr>

        <tr><td height="10"></td></tr>

    </table>
</form>
</body>
</html>