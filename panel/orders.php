<?php
session_start();
include_once("../include/config.php");
$main = new online_store();
if(!isset($_SESSION['admin_id'])){
    $main->redirect_html("index.php");
}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>نمایش اطاعات کاربران</title>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../javascript/main.js"></script>
</head>
<style type="text/css">
    body{
        font-family: 'B Nazanin' , Tahoma;
        background-color: #cccccc;
        font-size: 14pt;
    }

    a{
        text-decoration: none;
        color: black;
    }


</style>
<body>

<?php
if($task=='edit'){
    //edit
    $info = $main->info_users_admin($edit);
    if($_POST['save']){
        $email  = $_POST['email'];
        $name   = $_POST['name'];
        $family = $_POST['family'];
        $city   = $_POST['city'];
        $gender = $_POST['gender'];
        $phone  = $_POST['phone'];

        $rs = $main->Save_users($edit,$email,$name,$family,$phone);
        if($rs){
            $main->redirect_html("?msg=ok&page=$page&task=edit&edit=$edit");
        }else{
            $main->redirect_html("?msg=err&page=$page&task=edit&edit=$edit");
        }

    }




    ?>
    <table width="600" border="1" cellpadding="5" align="center" bordercolor="#333333">
        <br/><br/><br/>
        <tr align="center" style="font-size:24px"><td colspan="5">فاكتور خريد شما</td></tr>
        <tr bgcolor="#E8E8E8" align="center">
            <td width="100">  شماره</td>
            <td width="100">  نام محصول   </td>
            <td width="100">  قيمت محصول </td>
            <td width="100"> تعداد </td>
            <td width="100" >   قيمت كل محصول</td>


        </tr>

        <?php
        $result_u_b = $main->list_order_user_show_admin($_GET['s_id']);
        $i = 1;
        $sum[] = 0;
        $num_buy[] =0 ;

        while ($rows = mysqli_fetch_assoc($result_u_b)){
            $pro_info = $main->more_info_product($rows['pro_id']);
            $sum[]  =  ($pro_info['price'] * $rows['number']);
            $num_buy[] =    $rows['number'];
            ?>



            <tr align="center">
                <td width="100" bgcolor="#FFE6FF">  <?php print $i; ?>  </td>
                <td width="100"> <?php print $pro_info['title'] ?> </td>
                <td width="100">  <?php print $pro_info['price'] ?> </td>
                <td width="100"> <?php print $rows['number'] ?> </td>
                <td width="100" > <?php print ($pro_info['price'] * $rows['number']) ?>   </td>

            </tr>

            <?php
            $i++;
        }//end of while
        ?>



        <tr>
            <td align="center" colspan="5">   جمع كل با احتساب 5% تخفیف :
                <?php
                if($sum){
                    $total_buy =  array_sum($num_buy);
                    $total_sum = array_sum($sum);

                    $a = ($total_sum * 5) / 100;
                    $b = ($total_sum - $a);
                    print ceil($b);

                }
                ?></td>
        </tr>

    </table>



    <table  border="0" align="center">
        <tr align="center"><td><br/ >
                <a href="orders.php?page=<?php print $page ?>" title="بازگشت" style="font-size:16px" >بازگشت</a>

            </td></tr>
    </table>



    <?php
}else{
    //view
    $show = $main->Show_All_orders($page);
    if($_GET['del']=='del'){

        $rd = 	$main->del_orders($_GET['did']);
        if($rd){
            $main->redirect_html("?msg=ok_del&page=$page");
        }else{
            $main->redirect_html("?msg=err_del&page=$page");
        }

    }

    ?>
    <form method="post" action="">
        <table width="900" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center"><h2> نمایش سفارشات</h2> </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="center">
                    <?php

                    if($message=='ok_del'){
                        print " با موفقيت حذف شد";
                    }elseif($message=='err_del'){
                        print "با موفقيت حذف نشد";
                    }

                    ?>


                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">


                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="940">
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>


                            <td width="120" align="center">نام ونام  خانوادگی</td>
                            <td width="10"></td>
                            <td width="187" align="center">پست الكترنيك </td>
                            <td width="10"></td>
                            <td width="95" align="center">شماره تماس </td>
                            <td width="10"></td>
                            <td width="370" align="center">آدرس پستي</td>
                            <td width="5"></td>
                            <td width="127" align="center">عمليات</td>
                            <td width="6"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <?php
                        while ($rows = mysqli_fetch_assoc($show[0])){
                            ?>

                            <tr>


                                <td bgcolor="#DDF99B" align="center"><?php print $rows['fl_name'] ?></td>
                                <td width="10">
                                <td bgcolor="#DDF99B" align="center"><?php print $rows['email'] ?></td>
                                <td width="10">
                                <td bgcolor="#DDF99B" align="center"><?php print $rows['phone'] ?></td>
                                <td width="10">
                                <td bgcolor="#DDF99B" align="center"><?php print $rows['address'] ?></td>
                                <td width="5">

                                <td>
                                    <input type="button" value="حذف" class="input_button" onclick="del('?del=del&did=<?php print $rows['id'] ?>&page=<?php print $page ?>');" />
                                    <input type="button" value="مشاهده" class="input_button" onclick="redirect('?task=edit&page=<?php print $page ?>&s_id=<?php print $rows['session_id'] ?>');" />
                                </td>
                                <td width="6"></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <?php
                        }
                        ?>

                    </table>

                    <div style="height:20px"></div>
                    <div id="nav">
                        <?php
                        print " صفحه <b>$page</b>  از <b>$show[1]</b> ";
                        ?>
                        <div style="height:10px;"></div>
                        <?php
                        if($page < $show[1]){
                            $next = ($page + 1);
                            ?>
                            <img src="../images/next.jpg" height="25"  style="cursor:pointer" onclick="redirect('?page=<?php print $next ?>')" width="25" alt="صفحه بعدي" title="صفحه بعدي" />
                            <?php
                        }


                        ?>
                        <?php
                        if($page > 1){
                            $prev = ($page - 1);
                            ?>
                            &nbsp;
                            <img src="../images/back.jpg" height="25"  style="cursor:pointer" onclick="redirect('?page=<?php print $prev ?>')" width="25" alt="صفحه قبلي" title="صفحه قبلي" />
                            <?php
                        }
                        ?>

                    </div>
                    <div style="height:10px;"></div>

                    <input type="button" value="بازگشت" class="input_button" onclick="redirect('index.php');"  />


                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td  align="center">
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
        </table>
    </form>

    <?php
}
?>
</body>
</html>
