<?php
session_start();
include_once ("include/config.php");
include_once ("include/date.php");

$main = new online_store();

$main->all_visit();
$main->insert_temp_visit();



if (isset($_POST["register"])){
    $username               = $_POST["username"];
    $_SESSION["username"]   = $username;

    $password               = $_POST["pass1"];

    $name                   = $_POST["name"];
    $_SESSION["name"]       = $name;

    $last_name              = $_POST["last_name"];
    $_SESSION["last_name"]  = $last_name;

    $phone_number            = $_POST["phone"];
    $_SESSION["phone_number"]= $phone_number;

    $email                   = $_POST["email"];
    $_SESSION["email"]       = $email;

    $gender                  = $_POST["gender"];
    $_SESSION["gender"]      = $_POST["gender"];

    $city                    = $_POST["city"];
    $_SESSION["city"]        = $city;


    if ($username == ""){
        $main->redirect("index.php?msg=no_user&menu_id=2&page=1");
    }elseif($password == ""){
        $main->redirect("index.php?msg=no_pass&menu_id=2&page=1");
    }elseif($name == ""){
        $main->redirect("index.php?msg=no_name&menu_id=2&page=1");
    }elseif ($last_name == ""){
        $main->redirect("index.php?msg=no_lname&menu_id=2&page=1");
    }elseif ($phone_number == ""){
        $main->redirect("index.php?msg=no_phone&menu_id=2&page=1");
    }elseif (!filter_var($phone_number , FILTER_VALIDATE_INT)){
        $main->Redirect('index.php?msg=novalidphone&menu_id=2&page=1');
    }elseif ($email == ""){
        $main->redirect("index.php?msg=no_email&menu_id=2&page=1");
    }elseif (!filter_var($email , FILTER_VALIDATE_EMAIL)){
        $main->redirect("index.php?msg=no_emailval&menu_id=2&page=1");
    }elseif ($city == 0){
        $main->redirect("index.php?msg=no_city&menu_id=2&page=1");
    }else{

        $result = $main->register($username,$password,$name,$last_name,$phone_number,$email,$gender,$city);

        $_SESSION["user_id"] = $result;

        if($result == -1){
            $main->redirect("index.php?msg=error1");
        }else{
            $main->redirect("index.php?msg=success&uid= $result");
            session_unset();
            session_destroy();
        }
    }
}
?>

<!DOCTYPE html PUBLIC "~//W3C//DTD XHTML 1.0 Strict//EN">
<html lang="en">
<head xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script language="JavaScript" type="text/javascript" src="javascript/main.js"></script>

    <!-- start jquery and slide show -->
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link href="magicslideshow-trial/magicslideshow/magicslideshow.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="magicslideshow-trial/magicslideshow/magicslideshow.js" type="text/javascript"></script>

    <!-- End jquery and slide show -->
    <title>OnlineCarpetShop.ir</title>
</head>
<body>

<div>
    <div>

        <div class="MagicSlideshow" data-options="">
            <img src="slide_pic/1.jpg" alt="carpet"/>
            <img src="slide_pic/2.jpg" alt="carpet"/>
            <img src="slide_pic/3.jpg" alt="carpet"/>
            <img src="slide_pic/4.jpg" alt="carpet"/>
        </div>

    </div>



    <div  style="width: 100%">
        <?php

         include_once ("menu.php");

        ?>
    </div>
    <div>
        <span style="float: left; width: 78%; background-color: white; border: 1px solid black; border-radius: 10px; box-shadow: 5px 5px 5px 5px #2b2b2b;">
                        <?php
                        if ($menu_id == ""){
                            $page=1;
                        }
                        if ($task == "logout") {
                            session_unset();
                            session_destroy();
                            $main->redirect_html("index.php?menu_id=-3");
                        }


                        if ($menu_id == -1){

                            include_once ("profile.php");

                        //}elseif ($menu_id == -2){

                         //   include_once("recovery_password.php");

                        }elseif ($menu_id == -4){

                            include_once("show_basket.php");

                        }elseif ($menu_id == -5){

                            include_once("factor.php");

                        }elseif ($menu_id == -6){

                            include_once("find.php");

                        }elseif ($menu_id == 2 || $message == "success"|| $message == "no_user"|| $message == "no_pass"|| $message == "no_name"||
                            $message == "no_lname"|| $message == "no_phone"|| $message == "no_email"|| $message == "no_emailval"|| $message == "no_city"||
                            $message == "error1"){

                            include_once("register.php");

                        }elseif ($menu_id == 4){

                            include_once ("contact.php");

                        }elseif ($menu_id == 5){

                            include_once ("about_us.php");

                        }elseif ($menu_id == 6 || $menu_id == 3|| $menu_id == ""){

                            include_once ("product.php");

                        }
                        ?>
        </span>
        <span style="float: right; width: 20%; height:100% ;background-color: #80ffc9; border-radius: 10px;">
           <?php
           include_once ("right_content.php");
           ?>
        </span>

    </div>
</div>

</body>
</html>























<!--
<table align="center">
    <tr><td width="1300px"  height="100px" colspan="2">header</td></tr>
    <tr>
        <td width="1300px"  height="25px" colspan="2">
            <?php

// include_once ("menu.php");

?>
        </td>
    </tr>
    <tr>
        <td width="1100px" >
            <?php

// if ($task == "logout") {
//     session_unset();
//     session_destroy();
//     $main->redirect_html("index.php?menu_id=-3");
// }


// if ($menu_id == -1){

//     include_once ("profile.php");

// }elseif ($menu_id == -2){

//     include_once("recovery_password.php");

// }elseif ($menu_id == 2 || $message == "success"|| $message == "no_user"|| $message == "no_pass"|| $message == "no_name"||
//     $message == "no_lname"|| $message == "no_phone"|| $message == "no_email"|| $message == "no_emailval"|| $message == "no_city"||
//     $message == "error1"){

//     include_once("register.php");

// }elseif ($menu_id == 4){

//     include_once ("contact.php");

// }elseif ($menu_id == 5){

//     include_once ("about_us.php");

// }

?>
        </td>

        <td width="200px" style="height:  200px">
            <?php
//  include_once ("right_content.php");
?>
        </td>
    </tr>
    <tr>
        <td width="1300px"  height="100px" colspan="2" id="footer" >
            محتویات این سایت مطعلق به مصطفی کاشفی است<br>
            و هرگونه کپی برداری پیگرد قانونی دارد
        </td>
    </tr>

</table>

-->
<!--
<div align="right">
     <a href="index.php?menu_id=-3" title="login" style="text-decoration: none">ورود</a><br>
     <a href="index.php?menu_id=2" title="register" style="text-decoration: none">عضویت</a><br>
     <a href="index.php?menu_id=4" title="conect whit us" style="text-decoration: none">ارتباط با ما</a><br>
     <a href="index.php?menu_id=5" title="about us" style="text-decoration: none">درباره ما</a><br>
</div>
-->