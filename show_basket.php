<?php
if($task=='delete') {

    $del_basket = $main->delete_basket($delete);
    if ($del_basket) {

        $main->Redirect_html("index.php?menu_id=-4&msg=yes_delete");

    } else {

        $main->Redirect_html("index.php?menu_id=-4&msg=no_delete");

    }
}elseif ($task=='edit'){

    $up_basket = $main->update_basket($edit,$number);
    if ($up_basket) {

        $main->Redirect_html("index.php?menu_id=-4&msg=yes_update");

    } else {

        $main->Redirect_html("index.php?menu_id=-4&msg=no_update");

    }
}
?>


<script>
    function delete_basket(url) {
        var conf = confirm('آیا می خواهید این سطر را حذف کنید');
        if(conf) window.location.href =url;
    }
</script>


<h3 align="center" style="font-weight: bold;">سبد خرید شما</h3>
<table dir="rtl" align="center"  border="1" cellspacing="0" cellpadding="0" width="650" style="line-height:25px" valign="top">

    <tr align="center" style="font-weight: bold; font-size: 13pt;">
        <td> شماره </td>
        <td> نام محصول </td>
        <td> قیمت </td>
        <td> تعداد </td>
        <td> قیمت کل هر محصول </td>
        <td> عملیات </td>
    </tr>
    <?php
    $result_list_basket = $main->list_user_basket();
    $counter = 1;
    $sum[]=0;
    $number_buy[]=0;
    while ($rows = mysqli_fetch_assoc($result_list_basket)) {
         $info_pro = $main->more_info_product($rows['pro_id']);
         $sum[] = ($info_pro['price']*$rows['number']) ;
         $number_buy[] = $rows['number'] ;
    ?>
    <tr align="center">
        <td> <?php print $counter?> </td>
        <td> <?php print $info_pro['title'] ?> </td>
        <td> <?php print $info_pro['price'] ?> </td>
        <td> <input type="text" id="number_product_<?php print $rows["id"] ?>" name="number_product_<?php print $rows["id"] ?>" size="3" value="<?php print $rows['number'] ?>"> </td>
        <td> <?php print $info_pro['price']*$rows['number'] ?> </td>
        <td>
            <button name="delete" id="delete" onclick="delete_basket('index.php?menu_id=-4&task=delete&delete=<?php print $rows["id"] ?>')">حذف</button>
            <input type="button" value="ویرایش" id="number_product_<?php print $rows["id"] ?>"  onClick="redirect('index.php?menu_id=-4&task=edit&edit=<?php print $rows['id']?>&number='+document.getElementById('number_product_<?php print $rows["id"] ?>').value);">
        </td>
    </tr>
    <?php
        $counter++;
    }
    ?>
</table>
<div align="center" style="font-family: 'B Nazanin' , Tahoma ; font-size: 18pt">
    <span style="color: red" id="text">
        <?php

        if($message == "no_delete") {
            print "عملیات حذف صورت نگرفت";
        }?>
    </span>
    <span style="color: green">
        <?php

        if ($message == "yes_delete") {
            print "عملیات حذف با موفقیت صورت گرفت";
        }

        ?>
    </span>
</div>
<div align="center" style="font-family: 'B Nazanin' , Tahoma ; font-size: 18pt">
    <span style="color: red" id="text">
        <?php

        if($message == "no_update") {
            print "عملیات ویرایش صورت نگرفت";
        }?>
    </span>
    <span style="color: green">
        <?php

        if ($message == "yes_update") {
            print "عملیات ویرایش با موفقیت صورت گرفت";
        }

        ?>
    </span>
</div>

<div align="center" style="font-family: 'B Nazanin' , Tahoma ; font-size: 18pt; color:darkred ">
    <br>
<?php
$total_sum = array_sum($sum);
$total_number = array_sum($number_buy);

print "تعداد خرید شما : ".($total_number);
print "<br>";

if ($total_sum >= 5000000 || $total_number >= 5){
    $dic = ($total_sum*5)/100;
    print "مبلغ کل خرید شما با احتساب 5% تخفیف : ".($total_sum-$dic)." تومان";
}else{
    print "مبلغ کل خرید شما : ".($total_sum)." تومان";
}
?>
</div>
<br><br>
<div align="center">
    <input type="button" value="بازگشت به صفحه ی محصولات" onclick="redirect('index.php?menu_id=6&page=1')">
    <input type="button" value="ثبت سفارش" onclick="redirect('index.php?menu_id=-5')">
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div style="width: 100%; color: black; text-align: center; font-size: 10pt; font-weight: bold" >
    ---------------------------------------------------------------------<br>
    محتویات این سایت مطعلق به مصطفی کاشفی است<br>
    و هرگونه کپی برداری پیگرد قانونی دارد
</div>