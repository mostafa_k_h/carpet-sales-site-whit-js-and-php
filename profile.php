<?php
$user_info = $main->user_info($_SESSION["user_id"]);

if (isset($_POST["save"])) {
    $username     = $_POST["username"];
    $password     = $_POST["pass1"];
    $name         = $_POST["name"];
    $last_name    = $_POST["last_name"];
    $phone_number = $_POST["phone"];
    $email        = $_POST["email"];
    $gender       = $_POST["gender"];
    $city         = $_POST["city"];

    $result_profile = $main->update_profile($_SESSION['user_id'], $username, $password, $name, $last_name, $phone_number, $email, $gender, $city);


    if ($result_profile == -1){
        $main->redirect_html("index.php?menu_id=-1&msg=erroruser");
    }elseif($result_profile == -2){
        $main->redirect_html("index.php?menu_id=-1&msg=erroremail");
    }elseif($result_profile == -3){
        $main->redirect_html("index.php?menu_id=-1&msg=errorphone");
    }else {
        $main->redirect_html("index.php?menu_id=-1&msg=saveprof");
    }
}

?>
<div align="center"><b id="b1">فرم ویرایش اطلاعات</b></div>


<form method="post" onsubmit="">
    <table align="center" dir="rtl" id="t1">
        <tr><td><label for="username">نام کاربری :</label></td><td><input type="text" id="username" name="username" maxlength="50" value="<?php print $user_info["username"] ?>"></td><td class="star" id="star1">*</td></tr>
        <tr><td><label for="pass1">کلمه عبور :</label></td><td><input type="password" id="pass1" name="pass1" maxlength="20"></td><td class="star" id="star2">*</td></tr>
        <tr><td><label for="name">نام :</label></td><td><input type="text" id="name" name="name" maxlength="20" value="<?php print $user_info["name"] ?>"></td><td class="star" id="star4">*</td></tr>
        <tr><td><label for="last_name">نام خانوادگی :</label></td><td><input type="text" id="last_name" name="last_name" maxlength="20" value="<?php print $user_info["family"] ?>"></td><td class="star" id="star5">*</td></tr>
        <tr><td><label for="phone">تلفن تماس :</label></td><td><input type="text" id="phone" name="phone" maxlength="50" value="<?php print $user_info["phone"] ?>"></td><td class="star" id="star6">*</td></tr>
        <tr><td><label for="email">پست الکترونیکی :</label></td><td><input type="text" id="email" name="email" maxlength="50" value="<?php print $user_info["email"] ?>"></td><td class="star" id="star7">*</td></tr>
        <tr><td>جنسیت :</td>
            <td>
                <label>مرد :<input type="radio" id="gender1" name="gender" value="1" checked="checked"></label>
                <label>  زن :<input type="radio" id="gender2" name="gender" value="2" <?php
                    if ($user_info['gender'] == 2){
                        print 'checked="checked"';
                    }?>></label>
            </td>
            <td class="star" id="star8">*</td>
        </tr>
        <tr><td>شهر :</td>
            <td>
                <select id="city" name="city">
                    <option value="0">لطفا شهر خود را انتخاب کنید .</option>
                    <?php
                    $result_city = $main->all_city();
                    while ($cities = mysqli_fetch_assoc($result_city)){
                        ?>
                        <option
                            <?php if ($user_info["city"] == $cities["id"]) print "selected = selected"?>
                            value="<?php print $cities["id"] ?>"><?php print $cities["title"] ?>
                        </option>
                    <?php  } //end of while  ?>
                </select>
            </td>
            <td class="star" id="star9">*</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" id="save" name="save"  value="ذخیره">
                <input type="button" value="بازگشت" onclick="redirect('index.php?menu_id=-3')">
            </td>
        </tr>
    </table>
</form>
<div align="center" style="font-family: 'B Nazanin' , Tahoma ; font-size: 18pt">
    <span style="color: red" id="text">
 <?php

 if($message == "erroruser") {
     print "نام کاربری قبلا ثبت شده است";
 }elseif ($message == "erroremail") {
     print "پست الکترونیکی قبلا ثبت شده است";
 }elseif ($message == "errorphone") {
     print "شماره تلفن قبلا ثبت شده است";
 }
 ?>
     </span>
    <span style="color: green">
        <?php
        if ($message == "saveprof") {
            print "ویرایش اطلاعات با موفقیت صورت گرفت";
        }
        ?>
    </span>
</div>
