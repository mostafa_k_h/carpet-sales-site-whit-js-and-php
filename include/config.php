<?php
error_reporting(~E_NOTICE);
$message = $_GET['msg'];
$menu_id = $_GET['menu_id'];
$login   = $_GET['login'];
$task    = $_GET['task'];
$pid    = $_GET['pid'];
$delete    = $_GET['delete'];
$number    = $_GET['number'];
$edit    = $_GET['edit'];
$page    = $_GET['page'];
$title    = $_GET['title'];

class online_store
{
    public $db;

    public function __construct()
    {

        $this->db = mysqli_connect("localhost", "root", "", "online_store");
        if (mysqli_connect_errno()) {
            echo 'DB CONNECTION ERROR';
            die;
        } else {
            mysqli_query($this->db, 'SET NAMES UTF8');
        }
    }// End of function __construct

    public function __destruct()
    {
        mysqli_close($this->db);
    }// End of function __destruct

    function register($user, $pass, $name, $last_name, $phone_number, $email, $gender, $city)
    {

        $check_user_email = "SELECT * FROM `tbl_users` WHERE `username`='$user' OR `email`='$email'";
        $result_check_user_email = mysqli_query($this->db, $check_user_email);
        $user_alerdy = mysqli_fetch_assoc($result_check_user_email);

        if ($user_alerdy["id"]) {
            $final_result = -1;
        } else {
            $query = "INSERT INTO `tbl_users` (`id`, `username`, `password`, `name`, `family`, `phone`, `email`, `gender`, `city`)
                       VALUES (NULL, '$user', '$pass', '$name', '$last_name', '$phone_number', '$email', '$gender', '$city');";
            mysqli_query($this->db, $query);
            $final_result = mysqli_insert_id($this->db);
        }

        return $final_result;
    }// End of function register


    function redirect($page)
    {
        header("Location:$page");
    }// End of function redirect


    function redirect_html($page)
    {
        print "<script>window.location.href  = '$page';</script>";
    }// End of function redirect


    function all_city()
    {
        $query = "SELECT * FROM `tbl_city`";
        return mysqli_query($this->db, $query);
    }// End of function all_city

    function user_login($user, $pass)
    {

        $login_check = "SELECT * FROM `tbl_users` WHERE `username`='$user' AND `password`='$pass'";
        $result_login_check = mysqli_query($this->db, $login_check);
        $user_alerdy = mysqli_fetch_assoc($result_login_check);

        if ($user_alerdy["id"]) {

            $final_result = $user_alerdy["id"];
            $_SESSION["user_id"] = $final_result;

        } else {
            $final_result = 0;
        }

        return $final_result;
    }// End of function register

    function user_info($uid){
        $query = "SELECT * FROM `tbl_users` WHERE `id`='$uid'";
        $result_info = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result_info);

    }// End of function user_info

    function update_profile($uid,$username,$password,$name,$last_name,$phone_number,$email,$gender,$city){

        $user_info_profile = $this->user_info($uid);
        //`password` = '1' WHERE `tbl_users`.`id` =6

        $query = "UPDATE `tbl_users` SET ";
        $ret   = 1;

        if($user_info_profile['username']!=$username){

            $query_user       = "SELECT * FROM `tbl_users` WHERE `username`='$username'";
            $result_query_user= mysqli_query($this->db,$query_user );
            $user_alerdy      = mysqli_fetch_assoc($result_query_user);
            if($user_alerdy['username']==$username){

                $ret=-1;

            }else{
                $query.="`username` = '$username' ,";

            }

        }//end of if for user

        if($user_info_profile['email']!=$email){

            $query_email       = "SELECT * FROM `tbl_users` WHERE `email`='$email'";
            $result_query_email= mysqli_query($this->db,$query_email );
            $user_alerdy       = mysqli_fetch_assoc($result_query_email);
            if($user_alerdy['email']==$email){

                $ret=-2;

            }else{
                $query.="`email` = '$email' ,";

            }

        }//end of if for email

        if($user_info_profile['phone']!=$phone_number){

            $query_phone       = "SELECT * FROM `tbl_users` WHERE `phone`='$phone_number'";
            $result_query_phone= mysqli_query($this->db,$query_phone );
            $user_alerdy       = mysqli_fetch_assoc($result_query_phone);
            if($user_alerdy['phone']==$phone_number){

                $ret=-2;

            }else{
                $query.="`phone` = '$phone_number' ,";

            }

        }//end of if for phone

        if($password){
            $query.="`password` = '$password' ,";
        }

        if ($name != "" && $last_name != "" && $city != 0) {
            $query .= "`gender` = '$gender' ,`city` = '$city',`name` = '$name',`family` = '$last_name' where `id` ='$uid'";
        }
        mysqli_query($this->db,$query);

        return $ret;
    }// End of function update_profile


    function send_email($email){

        $query_email       = "SELECT * FROM `tbl_users` WHERE `email`='$email'";
        $result_query_email= mysqli_query($this->db,$query_email );
        return mysqli_fetch_assoc($result_query_email);

    }//End of function send_email


    function send_html_email($from,$to,$subject,$message){

        $header  = "from" . $from . "\n";
        $header .= "Reply-To: $to \n";
        $header .= "Mime-version: 1.0 \n";
        $header .= "Content-Type: text\html charset:utf-8 \n";
        $header .= "Content-Transfer-Encoding: 8bit \n";

       return mail($to,$subject,$message,$header);

    }//End of function send_html_email


    function main_menu()
    {
        $query = "SELECT * FROM `tbl_menu` where `sub_menu` = 0";
        return mysqli_query($this->db, $query);

    }// End of function main_menu

    function sub_menu($id)
    {
        $query = "SELECT * FROM `tbl_menu` where `sub_menu` = '$id'";
        return mysqli_query($this->db, $query);

    }// End of function sub_menu


    function insert_temp_visit(){

        $session_id = session_id();
        $date  = date("Y-m-d");

        $query = "SELECT * FROM `tbl_temp` where `session_id` = '$session_id'";
        $search_session = mysqli_query($this->db, $query);
        $result_search_session = mysqli_fetch_assoc($search_session);

        if ( $result_search_session ){

        }else {
            $query = "INSERT INTO `tbl_temp` VALUES (NULL, '$session_id', '$date')";
            mysqli_query($this->db, $query);
        }

    }// End of function insert_temp_visit

    function count_visit($day){
        $date  = date("Y-m-d");

        if ($day == 1){
            $query = "SELECT count(*) `count_visit` FROM `tbl_temp` where `date` = '$date'";
        }elseif ($day == 2){
            $query = "SELECT count(*) `count_visit` FROM `tbl_temp` where `date` = subdate('$date',interval 1 day)";
        }

          $search_date = mysqli_query($this->db, $query);
          $result_search_date = mysqli_fetch_assoc($search_date);
          return $result_search_date['count_visit'];

    }//End of class count_visit

    function all_visit(){

        $session_id = session_id();

        $query = "SELECT * FROM `tbl_temp` where `session_id` = '$session_id'";
        $search_session = mysqli_query($this->db, $query);
        $result_search_session = mysqli_fetch_assoc($search_session);

       if ( $result_search_session ){

       }else {
            $query = "UPDATE `tbl_visit` SET `visit`= `visit`+1 WHERE `id`=1";
            mysqli_query($this->db, $query);
       }

    }// End of function all_visit


    function select_all_visit(){

        $query = "SELECT `visit` FROM `tbl_visit` where  `id`=1";
        $search_all_visit = mysqli_query($this->db, $query);
        $result_search_all_visit = mysqli_fetch_assoc($search_all_visit);
        return $result_search_all_visit['visit'];

    }// End of function select_all_visit


    function product($page){
        $page = (int)$page;
        $max_len = 8 ;
        $from = (($page*$max_len)-$max_len);

        $query1 = "SELECT * FROM `tbl_product` where `show`='1' LIMIT $from,$max_len";

        $query2  = "SELECT  count(*)  `count_product` FROM `tbl_product` where `show`='1'";
        $result2 = mysqli_query($this->db, $query2);
        $result2_search = mysqli_fetch_assoc($result2);
        $result2_search_count = $result2_search['count_product'];
        $total_page = ceil($result2_search_count/$max_len);

        $result1 = mysqli_query($this->db, $query1);

        $ret[0] = $result1;
        $ret[1] = $total_page;

        return $ret;

    }// End of function product


    function more_info_product($id){

        $query  = "SELECT * FROM `tbl_product` where `id`='$id'";
        $result = mysqli_query($this->db, $query);
        return  mysqli_fetch_assoc($result);

    }// End of function more_info_product

    function save_in_basket($product_id,$number){

        $session_id = session_id();
        $query = "INSERT INTO `tbl_basket` VALUES (NULL, '$session_id','$product_id','$number')";
        return mysqli_query($this->db, $query);

    }// End of function save_in_basket

    function count_basket(){
        $session_id = session_id();

        $query = "SELECT count(*) `count_basket` FROM `tbl_basket` where `session_id` = '$session_id'";
        $search_basket = mysqli_query($this->db, $query);
        $result_search_basket = mysqli_fetch_assoc($search_basket);
        return $result_search_basket['count_basket'];

    }//End of class count_basket


    function list_user_basket(){
        $session_id = session_id();
        $query = "SELECT * FROM `tbl_basket` WHERE `session_id`='$session_id'";
        return mysqli_query($this->db , $query);

    }// End of function list_user_basket


    function delete_basket($id){
        $query = "DELETE FROM `tbl_basket` WHERE `id`='$id'";
        return mysqli_query($this->db , $query);

    }// End of function delete_basket


    function update_basket($id,$number){
        $query = "UPDATE `tbl_basket` SET `number`= '$number' WHERE `id`='$id'";
       return mysqli_query($this->db, $query);

    }// End of function update_basket

    function insert_order($full_name_factor,$email_factor,$phone_factor,$address_factor){
            $session_id = session_id();

        if ($full_name_factor=="" || $email_factor=="" ||$phone_factor=="" ||$address_factor==""){

        }else {
            $query = "INSERT INTO `tbl_order` VALUES (NULL, '$session_id','$full_name_factor','$email_factor','$phone_factor','$address_factor')";
            return mysqli_query($this->db, $query);
        }
    }// End of function update_basket


    function find_title($title){
        $query = "SELECT * FROM `tbl_product` WHERE `title` LIKE '%$title%'";
        return mysqli_query($this->db, $query);

    }// End of function find_title



    //------------------ admin ---------------------------------

    function Login_Admin($user,$pass){
        $query = "SELECT * FROM `tbl_admin` WHERE `username` = '$user' AND `password` = '$pass' ";
        $result =  mysqli_query($this->db,$query);
        $info =  mysqli_fetch_assoc($result);
        if($info['id']){
            //login
            $_SESSION['admin_id'] = $info['id'];
            $ret = $info['id'];
        }else{
            //no login
            $ret = 0;

        }
        return $ret;
    }//End of fun Login Admin

    //Info_Admin
    function Info_Admin($id){
        $id = (int) $id;
        $query  = "SELECT * FROM `tbl_admin` WHERE `id` = '$id' ";
        $result =  mysqli_query($this->db,$query);
        return mysqli_fetch_assoc($result);
    }//End of fun Info_Admin

    function save_profile_admin($id,$user,$pass){
        $id = (int) $id;
        if ($user=="" || $pass==""){

        }else {
            $query = "UPDATE `tbl_admin` SET `username` = '$user' , `password` = '$pass' WHERE `id` = '$id' ";
            return mysqli_query($this->db, $query);
        }
    }//End of fun save_profile_admin


//upload File
    function Upload_File(){
        $path=getcwd().DIRECTORY_SEPARATOR."../data";

        $file= $_FILES['file']['tmp_name'];
        $filename= $_FILES['file']['name'];
        $typefile= $_FILES['file']['type'];
        $filename = strtolower($filename);
        // check exetions
        if(!strstr($filename,'jpg') && !strstr($filename,'png') && !strstr($filename,'gif')){
            //file exetion not valid
            $retupload=1;
        }else{
            // type file
            if($typefile=='image/x-png' || $typefile =='image/png'){
                $exe = ".png";
            }elseif($typefile=='image/jpeg' || $typefile=='image/pjpeg'){
                $exe = ".jpg";
            }elseif($typefile=='image/gif'){
                $exe = ".gif";
            }
            //new name
            $newfilename=rand(111111,999999).$exe;
            //upload file
            if(!move_uploaded_file($file,$path.DIRECTORY_SEPARATOR.$newfilename)){
                //file not upload
                $retupload=2;
            }else{
                //file upload & file name & update pic user
                $retupload=$newfilename;
            }//end of if
        }//eind of if
        return $retupload;

    }//End of fun upload File


    function info_pro_admin($id){
        $id = (int) $id;
        $query = "SELECT * FROM `tbl_product` WHERE `id` = '$id' ";
        $r =  mysqli_query($this->db,$query);
        return mysqli_fetch_assoc($r);

    }//End of fun info_pro_admin


    //Save_pro
    function Save_pro($id,$title,$price,$disc1,$disc2,$pic){
        $id = (int) $id;

        $query = "UPDATE `tbl_product` SET `title` = '$title' , `price` = '$price', `abstract_dis` = '$disc1'
                  , `total_dis` = '$disc2', `pic` = '$pic' , `number` = '100' WHERE `id` = '$id' ";

        return mysqli_query($this->db,$query);
    }//end of fun Save_pro


    //del_pro
    function del_pro($id){
        $id = (int) $id;
        $query = "DELETE FROM `tbl_product` WHERE `id` = '$id'";
        return mysqli_query($this->db,$query);

    }//End of fun del_pro


    //Show_All_pro
    function Show_All_pro(){
        $query ="SELECT * FROM `tbl_product`";
        return mysqli_query($this->db,$query);

    }//End of fun Show_All_pro


    public function info_users_admin($id){
        $id = (int) $id;
        $query = "SELECT * FROM `tbl_users` WHERE `id` = '$id' ";
        $r =  mysqli_query($this->db,$query);
        return mysqli_fetch_assoc($r);

    }//End of fun info_users_admin


    //Save_users
    public function Save_users($id,$email,$name,$family,$phone){
        $id = (int) $id;
        $query = "UPDATE `tbl_users` SET `email` = '$email' , `name` = '$name', `family` = '$family' ,`phone` = '$phone' 
			      WHERE `id` = '$id' ";
        return mysqli_query($this->db,$query);

    }//End of fun Save_users


    //Show_All_users
    public function Show_All_users($page){
        $page = (int) $page;
        $max_results = 10;
        $from = (($page * $max_results) - $max_results);

        $query1 ="SELECT * FROM `tbl_users` order by `id` LIMIT $from, $max_results";

        $query2 = "SELECT COUNT(*) `count_user` FROM `tbl_users`";

        $result2=mysqli_query($this->db,$query2);
        $result2_search=mysqli_fetch_assoc($result2);
        $result2_search_count =  $result2_search['count_user'];
        $total_pages = ceil($result2_search_count / $max_results);

        $result1=mysqli_query($this->db,$query1);

        $ret[0]=$result1;
        $ret[1]=$total_pages;
        return $ret;



    }//End of fun Show_All_users


    //del_users
    public function del_users($id){
        $id = (int) $id;
        $query = "DELETE FROM `tbl_users` WHERE `id` = '$id'";
        return mysqli_query($this->db,$query);

    }//End of fun del_users

    //list_order_user_show_admin
    public function list_order_user_show_admin($s_id){
        $query = "SELECT * FROM `tbl_basket` WHERE `session_id` = '$s_id'";
        return mysqli_query($this->db,$query);

    }//End of fun list_order_user_show_admin


    //Show_All_orders
    public function Show_All_orders($page){
        $page = (int) $page;
        $max_results = 20;
        $from = (($page * $max_results) - $max_results);

        $query1 ="SELECT * FROM `tbl_order` order by `id`  LIMIT $from, $max_results";

        $query2 = "SELECT COUNT(*) `count_order` FROM `tbl_order`";

        $result2=mysqli_query($this->db,$query2);
        $result2_search=mysqli_fetch_assoc($result2);
        $result2_search_count =  $result2_search['count_order'];
        $total_pages = ceil($result2_search_count / $max_results);

        $result1=mysqli_query($this->db,$query1);

        $ret[0]=$result1;
        $ret[1]=$total_pages;

        return $ret;

    }//End of fun Show_All_orders


    //del_orders
    public function del_orders($id){
        $id = (int) $id;
        $query = "DELETE FROM `tbl_order` WHERE `id` = '$id'";
        return mysqli_query($this->db,$query);

    }//end of func del_orders



}//End of class online_shop

